/*jslint rhino: true */
/*global require console */

// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
"use strict";
var tabs = require("sdk/tabs");
var ui = require("sdk/ui");
var notifications = require("sdk/notifications");

var buttonShown = false,
    knownURLs = new Map(), deDupWidget = {};

function cleanURL(fullURL) {
    // strip out an anchored part ... same URL with different anchor
    // is same for us
    return fullURL.split("#", 1)[0];
}

function keys(dict) {
    var out = [];
    for (var idx in dict) {
        out.push(idx);
    }
    return out;
}

function removedupes() {
    var count = 0;

    for (var [idxURL, curURL] of knownURLs) {
        if (curURL.length > 1) {
            curURL.slice(1).forEach(function(t) {
                if (t === tabs.activeTab) {
                    curURL[0].activate();
                }
                t.close();
                count++;
            });
            curURL = new Array(curURL[0]);
        }
    }

    notifications.notify({
        title: "Deduplicate Tabs",
        text: "Removed " + count + " tabs."
    });

    refreshKnownURLs();
}

function refreshKnownURLs() {
    var url = "",
    duplicates = false;
    knownURLs.clear();

    Array.forEach(tabs, function (tab) {
        url = cleanURL(tab.url);
        if (url === "about:blank") {
            return;
        }
        if (knownURLs.has(url)) {
            knownURLs.get(url).push(tab);
        } else {
            knownURLs.set(url, new Array(tab));
        }
    });

    for(var [idx, urltab] of knownURLs) {
        if (urltab.length > 1) {
            duplicates = true;
            break ;
        }
    }

    if (duplicates) {
        if (!buttonShown) {
      deDupWidget = ui.ActionButton({
          id: 'dedup-action-widget',
          label: 'deDup',
          content: '<span style="font-weight: bold; color: navy;' +
          ' background-color: #ededed; font-size: small; cursor: pointer;">' +
          'deDup</span>',
        width: 65,
        icon: {
          "16": "./icon-16.png",
          "32": "./icon-32.png",
          "64": "./icon-64.png"
        },
        onClick: removedupes
      });
            buttonShown = true;
        }
    } else {
        if (buttonShown) {
            deDupWidget.destroy();
            buttonShown = false;
        }
    }
}

tabs.on('ready', function onOpen(tab) {
  refreshKnownURLs();
});
tabs.on('close', function onOpen(tab) {
  refreshKnownURLs();
});
